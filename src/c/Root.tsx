import { onMount } from "solid-js"
import typescriptLogo from "../typescript.svg"
import viteLogo from "/vite.svg"
import zdoginit from "../zdogtest"
import Hello from "./hello"

export default () => {
  const canvas = <canvas></canvas> as HTMLCanvasElement
  onMount(() => {
    zdoginit(canvas)
  })
  return (
    <div>
      <Hello/>
      <a href="https://vitejs.dev" target="_blank">
        <img src={viteLogo} class="logo" alt="Vite logo" />
      </a>
      <a href="https://www.typescriptlang.org/" target="_blank">
        <img src={typescriptLogo} class="logo vanilla" alt="TypeScript logo" />
      </a>
      <h1>Vite + TypeScript</h1>
      {canvas}
      <p class="read-the-docs">
        Click on the Vite and TypeScript logos to learn more
      </p>
    </div>
  )
}
