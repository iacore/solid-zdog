import './style.css'
import { render } from 'solid-js/web'
import Root from './c/Root'

render(() => <Root />, document.querySelector<HTMLDivElement>('#app')!)
